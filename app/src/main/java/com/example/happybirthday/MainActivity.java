package com.example.happybirthday;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TextView showGratz = (TextView) findViewById(R.id.show_gratz);
        TextView showSignature = (TextView) findViewById(R.id.show_signature);
        String gratz = getIntent().getExtras().get("gratz").toString();
        showGratz.setText(gratz);
        String signature = getIntent().getExtras().get("signature").toString();
        showSignature.setText(signature);
    }


}