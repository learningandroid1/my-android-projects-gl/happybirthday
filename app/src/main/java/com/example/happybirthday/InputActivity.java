package com.example.happybirthday;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class InputActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input);
    }

    public void preview(View v) {
        EditText gratz = (EditText) findViewById(R.id.gratz);
        EditText signature = (EditText) findViewById(R.id.signature);
        String enteredGratz = gratz.getText().toString();
        String enteredSignature = signature.getText().toString();
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("gratz", enteredGratz);
        intent.putExtra("signature", enteredSignature);
        startActivity(intent);


    }
}